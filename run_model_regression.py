# Import python libraries
import os, pathlib, json, click, time
import pandas as pd
# Import own modules
import vectorGC.regression_framework.models as models
from vectorGC.regression_framework.solver import scipy_solver

def generate_save_default(ctx, param, value):
    if value is None:
        # Access the values of other options using the context object (ctx)
        model = ctx.params.get('model')
        normalize = ctx.params.get('normalize')
        if model!="dipole":
            return f'{model}_{normalize}'
        else:
            return model
    return value

@click.command()
@click.option('--model', type=click.Choice(["vectorGC","muzeroGC"], case_sensitive=True), help='Name of the model.')
@click.option('--normalize', type=click.Choice(["sqrt","none"], case_sensitive=True), help='Method for normalizing data.')
@click.option('--project', type=click.STRING, help='Name of the data/project folder.')
@click.option('--save', callback=generate_save_default, type=click.STRING, help='Filename for saving resulting model.')
def main(model, normalize, project, save):
    # Check inputs
    if model is None:
        raise ValueError("Given model is None. Please choose an implemented model.")
    if normalize is None:
        raise ValueError("Given method for normalization is None. Please choose an implemented method.")
    if not os.path.exists(f'{pathlib.Path(__file__).parent.resolve()}/resources/processed_data/{project}'):
        raise ValueError("Given project folder does not exist.")
    
    # Get data
    group_info, chem_id, phys_data = load_data(project)

    # Check if model already exists
    path2outdir = f"{pathlib.Path(__file__).parent.resolve()}/output/models/{project}"
    if os.path.isfile(f"{path2outdir}/{save}.pkl"):
        raise ValueError("Given model file name exists already. Please check the inputs.")
    
    m = getattr(models, '{}'.format(model))(chem_id, group_info, phys_data=phys_data, normalize=normalize)

    # Get solver
    solver = scipy_solver(show_progress=False)

    # Perform regression for model
    print("--- Start regression")
    start_time = time.time()
    solver.fit(model=m)
    print("--- Regression done: %s seconds ---" % (time.time() - start_time))
    ## Get regression results
    m.get_fit_results()

    # Perform leave-substance-out cross-validation
    print("--- Start cross validation")
    start_time = time.time()
    solver.loocv(model=m)
    print("--- Cross validation done: %s seconds ---" % (time.time() - start_time))
    ## Get loocv results
    m.get_loocv_results()

    # Save model
    if not os.path.isdir(path2outdir):
        os.mkdir(path2outdir)
    m.save("{}/{}".format(path2outdir,save))

def load_data(project_name):
    with open('{0}/resources/processed_data/{1}/{1}_groups_info.json'.format(pathlib.Path(__file__).parent.resolve(), project_name), "r") as f:
        group_info = json.load(f)
    chem_id = pd.read_json('{0}/resources/processed_data/{1}/{1}_chem_id.json'.format(pathlib.Path(__file__).parent.resolve(), project_name)).set_index("mol_id")
    phys_data = pd.read_json('{0}/resources/processed_data/{1}/{1}_vap_den_data.json'.format(pathlib.Path(__file__).parent.resolve(), project_name)).set_index("mol_id")

    return group_info, chem_id, phys_data

if __name__ == '__main__':
    main()