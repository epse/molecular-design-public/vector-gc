from rdkit.Chem import AllChem as Chem
import numpy as np

default_smarts = {
    # NON-POLAR, NON-ASSOCIATING ALIPHATIC FUNCTIONAL GROUPS
    # alkanes -CH3 : no ring, no ethane, no methanol, no methylamine
    "-CH3": "[CH3;!R;!$([CH3][CH3]);!$([CH3][OH]);!$([CH3][NH2])]",
    # alkanes -CH2- : no ring
    "-CH2-": "[$([CX4H2][C]);!R;!$([CH2]=O);!$([CX4H2][F,Cl,Br,I])]",
    # alkanes >CH- : no ring
    ">CH-": "[$([CX4H1][C]);!R;!$([CH2]=O);!$([CX4H1][F,Cl,Br,I]);!$([CX4H1]([F,Cl,Br,I])[F,Cl,Br,I])]",
    # alkanes >C< : no ring
    ">C<": "[$([CX4H0][C]);!R;!$([CX4H0][F,Cl,Br,I]);!$([CX4H0]([F,Cl,Br,I])[F,Cl,Br,I]);!$([CX4H0]([F,Cl,Br,I])([F,Cl,Br,I])[F,Cl,Br,I])]",
    # alkenes =CH2 : no ring, no ethylene, no formaldehyde
    "=CH2": "[CX3H2;!R;!$([CH2]=[CH2]);!$([CH2]=O)]",
    # alkenes =CH- : no ring, no aldehyde
    "=CH-": "[CX3H1;!R;!$([CX3H1]=O);!$([CH2]=O);!$([CX3H1][F,Cl,Br,I])]",
    # alkenes >C= : no ring, no ketone
    ">C=": "[CX3H0;!R;!$([CX3H0]=O);!$([CH2]=O);!$([CX3H0][F,Cl,Br,I]);!$([CX3H0]([F,Cl,Br,I])[F,Cl,Br,I])]",
    # halogenated alkanes >C< : no ring
    ">C<_Halog": "[$([CX4H0][F,Cl,Br,I]);!R]",
    # halogenated alkanes -CH2- : no ring
    "-CH2-_Halog": "[$([CX4H2][F,Cl,Br,I]);!R]",
    # halogenated alkanes >CH- : no ring
    ">CH-_Halog": "[$([CX4H1][F,Cl,Br,I]);!R]",
    # halogenated alkenes >C= : no ring, no ketone
    ">C=_Halog": "[$([CX3H0][F,Cl,Br,I]);!R;!$([CX3H0]=O);!$([CH2]=O)]",
    # halogenated alkenes =CH- : no ring, no aldehyde
    "=CH-_Halog": "[$([CX3H1][F,Cl,Br,I]);!R;!$([CX3H1]=O)]",
    #
    # POLAR GROUPS
    # OXYGENATED
    # aldehydes -CH=O : neighbor has to be C
    "CH=O": "[$([CH1][C])]=O",
    # ketones >C=O : both neighbors have to be C, no rings
    ">C=O": "[$([C]([C])([C]));!R]=O",
    # ethers -O- : both neighbors have to be C, no rings, no formates/esters
    "O": "[$([O]([C])[C]);!R;!$([O][C]=O)]",
    # formates -OCH=O, neighbor has to be C
    "-O-CH=O": "[CH1](=O)[$([OH0][C])]",
    # esters -OC(=O)- : both neighbors have to be C, no rings
    "-O-(C=O)-": "[$([CH0][C]);!R](=O)[$([OH0]([C])[C])]",
    #
    # HALOGENS
    # fluorine F : neighbor has to be C
    "F": "[$(F[C])]",
    # chlorine Cl : neighbor has to be C
    "Cl": "[$(Cl[C])]",
    # bromine Br : neighbor has to be C
    "Br": "[$(Br[C])]"
}

default_bonds = {
    # CARBON - CARBON bonds with different hybridization
    "Csp2-Csp3" : ('Csp2','Csp3',1.0),

    # CARBON - HYDROGEN
    "C-H" : ('C','H',1.0),

    # CARBON - OXYGEN bonds
    # Single
    "O-C" : ('O','C',1.0),
    # Double
    "O=C" : ('O','C',2.0),

    # CARBON - HALOGEN bonds
    # Fluorine
    "F-C" : ('F','C',1.0),
    # Chlorine
    "Cl-C" : ('Cl','C',1.0),
    # Bromine
    "Br-C" : ('Br','C',1.0)
}

def fragment_molecule(smiles, smarts=default_smarts, bonds=default_bonds, nConf=3):
    mol = Chem.MolFromSmiles(Chem.MolToSmiles(Chem.MolFromSmiles(smiles)))
    atoms = mol.GetNumHeavyAtoms()

    # find the location of all fragment using the given smarts
    matches = dict(
        (g, mol.GetSubstructMatches(Chem.MolFromSmarts(smarts[g]))) for g in smarts
    )
    bond_list = [(b.GetBeginAtomIdx(), b.GetEndAtomIdx()) for b in mol.GetBonds()]
    fragments = convert_matches(atoms, matches, bond_list)

    unitvectors = get_unitvectors(mol, nConf, bonds)
    return fragments, unitvectors

def convert_matches(atoms, matches, bonds):
    # check if every atom is captured by exatly one fragment
    identified_atoms = [i for key, l in matches.items() if "SO:" not in key for k in l for i in k]
    unique_atoms = set(identified_atoms)
    if len(unique_atoms) == len(identified_atoms) and len(unique_atoms) == atoms:
        # Translate the atom indices to segment indices (some segments contain more than one atom)
        segment_indices = sorted(
            (sorted(k), group) for group, l in matches.items() for k in l
        )
        segments = [group for _, group in segment_indices]
        segment_map = [i for i, (k, _) in enumerate(segment_indices) for j in k]
        bonds = [(segment_map[a], segment_map[b]) for a, b in bonds]
        bonds = [(a, b) for a, b in bonds if a != b]
        return segments, bonds
    raise Exception("Molecule cannot be fragmented with the given SMARTS!")

def get_unitvectors_old(mol, nConf, bonds):
    molh = Chem.AddHs(mol)
    Chem.EmbedMultipleConfs(molh, numConfs=nConf, randomSeed=0xf00d) 
    Chem.MMFFOptimizeMolecule(molh)

    unitvectors = {i : {} for i in range(nConf)}
    bonds = dict((k,tuple(v)) for k,v in bonds.items())
    bonds_ = dict((v,k) for k,v in bonds.items())

    atoms = {}
    for a in molh.GetAtoms():
        a_idx = a.GetIdx()
        atoms[a_idx] = {"symbol" : a.GetSymbol(), "hybridization" : "", "coordinates" : {i : molh.GetConformers()[i].GetAtomPosition(a_idx) for i in range(nConf)}}
        if atoms[a_idx]["symbol"] == "C":
            if a.GetHybridization() == Chem.HybridizationType(4):
                atoms[a_idx]["hybridization"] = "sp3"
            elif a.GetHybridization() == Chem.HybridizationType(3):
                atoms[a_idx]["hybridization"] = "sp2"
            elif a.GetHybridization() == Chem.HybridizationType(2):
                atoms[a_idx]["hybridization"] = "sp"

    for b in molh.GetBonds():
        begin_atom = {"idx" : b.GetBeginAtomIdx(), "symbol" : atoms[b.GetBeginAtomIdx()]["symbol"]}
        end_atom = {"idx" : b.GetEndAtomIdx(), "symbol" : atoms[b.GetEndAtomIdx()]["symbol"]}
        if begin_atom["symbol"] == "C" and end_atom["symbol"] == "C":
            begin_atom["symbol"] += atoms[begin_atom["idx"]]["hybridization"]
            end_atom["symbol"] += atoms[end_atom["idx"]]["hybridization"]
        #
        if begin_atom["symbol"] == end_atom["symbol"]:
            continue
        # 
        bond_tuple = [bond_tuple for bond_tuple in bonds_ if (begin_atom["symbol"] in bond_tuple and end_atom["symbol"] in bond_tuple and b.GetBondTypeAsDouble()==bond_tuple[2])]
        if bond_tuple:
            bond_tuple = bond_tuple[0]
            for conf in range(nConf):
                vec = np.array([
                    atoms[end_atom["idx"]]["coordinates"][conf].x - atoms[begin_atom["idx"]]["coordinates"][conf].x,
                    atoms[end_atom["idx"]]["coordinates"][conf].y - atoms[begin_atom["idx"]]["coordinates"][conf].y,
                    atoms[end_atom["idx"]]["coordinates"][conf].z - atoms[begin_atom["idx"]]["coordinates"][conf].z
                    ])
                vec /= np.linalg.norm(vec)
                if end_atom["symbol"] == bond_tuple[0]:
                    vec *= -1.0
                if bonds_[bond_tuple] not in unitvectors[conf]:
                    unitvectors[conf][bonds_[bond_tuple]] = vec
                else:
                    unitvectors[conf][bonds_[bond_tuple]] += vec
        else:
            Exception("Molecule cannot be fragmented with the given bonds!")
        
    return unitvectors

def get_unitvectors_confs(mol, nConf, bonds, rmsd_cutoff=2.0):
    molh = Chem.AddHs(mol)

    # Create possible conformers and optimize geometries with force field, sort after ascending energy
    n_rot_bonds = Chem.CalcNumRotatableBonds(molh)
    n_conf = 50 if n_rot_bonds <= 7 else (200 if 7<n_rot_bonds<13 else 300)
    cids = Chem.EmbedMultipleConfs(molh, numConfs=n_conf, randomSeed=0xf00d)
    res = Chem.MMFFOptimizeMoleculeConfs(molh)

    rmslist = []
    Chem.AlignMolConformers(molh, RMSlist=rmslist)

    c_gen = list(range(len(cids)))
    c_keep = []
    c_notconverged = []
    try:
        c_min, min_energy = next((idx, e[1]) for idx, e in enumerate(res) if e[0]==0)
        for idx, entry in enumerate(res):
            if entry[0]==0 and entry[1] < min_energy:
                c_min = idx
                min_energy = entry[1]
            elif entry[0]==1:
                c_notconverged.append(idx)
    except:
        c_min, min_energy = (0, res[0][1])
        for idx, entry in enumerate(res):
            if entry[1] < min_energy:
                c_min = idx
                min_energy = entry[1]
    c_keep.append(c_min)
    c_gen.remove(c_min)
    for c in c_notconverged:
        c_gen.remove(c)

    for c in c_gen:
        rmsdlist = [Chem.GetConformerRMS(molh, c_ref, c) for c_ref in c_keep]
        if all(rmsd>rmsd_cutoff for rmsd in rmsdlist):
            c_keep.append(c)
    
    energies = [res[c][1] for c in c_keep]
    sorted_ids = [i[0] for i in sorted(enumerate(energies), key=lambda x:x[1])]
    sorted_c_keep = [c_keep[i] for i in sorted_ids]

    # Compute unit vectors for nConf conformers with lowest energy
    unitvectors = {i : {} for i in range(nConf)}
    bonds = dict((k,tuple(v)) for k,v in bonds.items())
    bonds_ = dict((v,k) for k,v in bonds.items())

    atoms = {}
    for a in molh.GetAtoms():
        a_idx = a.GetIdx()
        atoms[a_idx] = {"symbol" : a.GetSymbol(), "hybridization" : "", "coordinates" : {i : molh.GetConformers()[sorted_c_keep[i]].GetAtomPosition(a_idx) for i in range(min(len(sorted_c_keep),nConf))}}
        if atoms[a_idx]["symbol"] == "C":
            if a.GetHybridization() == Chem.HybridizationType(4):
                atoms[a_idx]["hybridization"] = "sp3"
            elif a.GetHybridization() == Chem.HybridizationType(3):
                atoms[a_idx]["hybridization"] = "sp2"
            elif a.GetHybridization() == Chem.HybridizationType(2):
                atoms[a_idx]["hybridization"] = "sp"

    for b in molh.GetBonds():
        begin_atom = {"idx" : b.GetBeginAtomIdx(), "symbol" : atoms[b.GetBeginAtomIdx()]["symbol"]}
        end_atom = {"idx" : b.GetEndAtomIdx(), "symbol" : atoms[b.GetEndAtomIdx()]["symbol"]}
        if begin_atom["symbol"] == "C" and end_atom["symbol"] == "C":
            begin_atom["symbol"] += atoms[begin_atom["idx"]]["hybridization"]
            end_atom["symbol"] += atoms[end_atom["idx"]]["hybridization"]
        #
        if begin_atom["symbol"] == end_atom["symbol"]:
            continue
        # 
        bond_tuple = [bond_tuple for bond_tuple in bonds_ if (begin_atom["symbol"] in bond_tuple and end_atom["symbol"] in bond_tuple and b.GetBondTypeAsDouble()==bond_tuple[2])]
        if bond_tuple:
            bond_tuple = bond_tuple[0]
            for conf in range(min(len(sorted_c_keep),nConf)):
                vec = np.array([
                    atoms[end_atom["idx"]]["coordinates"][conf].x - atoms[begin_atom["idx"]]["coordinates"][conf].x,
                    atoms[end_atom["idx"]]["coordinates"][conf].y - atoms[begin_atom["idx"]]["coordinates"][conf].y,
                    atoms[end_atom["idx"]]["coordinates"][conf].z - atoms[begin_atom["idx"]]["coordinates"][conf].z
                    ])
                vec /= np.linalg.norm(vec)
                if end_atom["symbol"] == bond_tuple[0]:
                    vec *= -1.0
                if bonds_[bond_tuple] not in unitvectors[conf]:
                    unitvectors[conf][bonds_[bond_tuple]] = vec
                else:
                    unitvectors[conf][bonds_[bond_tuple]] += vec
        else:
            Exception("Molecule cannot be fragmented with the given bonds!")
        
    return unitvectors

def get_unitvectors(mol, nConf, bonds):
    molh = Chem.AddHs(mol)

    Chem.EmbedMolecule(molh, randomSeed=0xf00d)
    Chem.MMFFOptimizeMolecule(molh)

    unitvectors = {0 : {}}
    bonds = dict((k,tuple(v)) for k,v in bonds.items())
    bonds_ = dict((v,k) for k,v in bonds.items())

    atoms = {}
    for a in molh.GetAtoms():
        a_idx = a.GetIdx()
        atoms[a_idx] = {"symbol" : a.GetSymbol(), "hybridization" : "", "coordinates" : {0 : molh.GetConformers()[0].GetAtomPosition(a_idx)}}
        if atoms[a_idx]["symbol"] == "C":
            if a.GetHybridization() == Chem.HybridizationType(4):
                atoms[a_idx]["hybridization"] = "sp3"
            elif a.GetHybridization() == Chem.HybridizationType(3):
                atoms[a_idx]["hybridization"] = "sp2"
            elif a.GetHybridization() == Chem.HybridizationType(2):
                atoms[a_idx]["hybridization"] = "sp"

    for b in molh.GetBonds():
        begin_atom = {"idx" : b.GetBeginAtomIdx(), "symbol" : atoms[b.GetBeginAtomIdx()]["symbol"]}
        end_atom = {"idx" : b.GetEndAtomIdx(), "symbol" : atoms[b.GetEndAtomIdx()]["symbol"]}
        if begin_atom["symbol"] == "C" and end_atom["symbol"] == "C":
            begin_atom["symbol"] += atoms[begin_atom["idx"]]["hybridization"]
            end_atom["symbol"] += atoms[end_atom["idx"]]["hybridization"]
        #
        if begin_atom["symbol"] == end_atom["symbol"]:
            continue
        # 
        bond_tuple = [bond_tuple for bond_tuple in bonds_ if (begin_atom["symbol"] in bond_tuple and end_atom["symbol"] in bond_tuple and b.GetBondTypeAsDouble()==bond_tuple[2])]
        if bond_tuple:
            bond_tuple = bond_tuple[0]
            vec = np.array([
            atoms[end_atom["idx"]]["coordinates"][0].x - atoms[begin_atom["idx"]]["coordinates"][0].x,
            atoms[end_atom["idx"]]["coordinates"][0].y - atoms[begin_atom["idx"]]["coordinates"][0].y,
            atoms[end_atom["idx"]]["coordinates"][0].z - atoms[begin_atom["idx"]]["coordinates"][0].z
            ])
            vec /= np.linalg.norm(vec)
            if end_atom["symbol"] == bond_tuple[0]:
                vec *= -1.0
            if bonds_[bond_tuple] not in unitvectors[0]:
                unitvectors[0][bonds_[bond_tuple]] = vec
            else:
                unitvectors[0][bonds_[bond_tuple]] += vec
        else:
            Exception("Molecule cannot be fragmented with the given bonds!")
        
    return unitvectors

def fragment_molecule_gcSauer(smiles, smarts=default_smarts):
    mol = Chem.MolFromSmiles(Chem.MolToSmiles(Chem.MolFromSmiles(smiles)))
    atoms = mol.GetNumHeavyAtoms()

    # find the location of all fragment using the given smarts
    matches = dict(
        (g, mol.GetSubstructMatches(Chem.MolFromSmarts(smarts[g]))) for g in smarts
    )
    bond_list = [(b.GetBeginAtomIdx(), b.GetEndAtomIdx()) for b in mol.GetBonds()]
    fragments = convert_matches(atoms, matches, bond_list)

    return convert_to_sauer_groups(fragments[0], fragments[1])

def convert_to_sauer_groups(segments, bonds):
    ethers = sum(1 for group in segments if group == "O")
    if ethers == 0:
        return segments, bonds
    elif ethers >= 1:
        #all_index = [i for i, group in enumerate(segments) if group == "O"]
        #for index in all_index:
        index = sum(i for i, group in enumerate(segments) if group == "O")
        neighbors = []
        for a, b in bonds:
            if a == index:
                right = (b, segments[b])
            elif b == index:
                left = (a, segments[a])

        if left[1] == "CH3":
            index2 = left[0]
            new_group = "OCH3"
        elif right[1] == "CH3":
            index2 = right[0]
            new_group = "OCH3"
        elif left[1] == "CH2":
            index2 = left[0]
            new_group = "OCH2"
        elif right[1] == "CH2":
            index2 = right[0]
            new_group = "OCH2"
        else:
            raise Exception(
                "The ether is not compatible with the groups by Sauer et al.!"
            )
        matches = {new_group: ((index, index2),)}
        for i, group in enumerate(segments):
            if i in {index, index2}:
                continue
            if group in matches:
                matches[group] = matches[group] + ((i,),)
            else:
                matches[group] = ((i,),)
        return convert_matches(len(segments), matches, bonds)
    else:
        raise Exception(
            "The conversion is only implemented for molecules with one ether group!"
        )
