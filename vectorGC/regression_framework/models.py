from .fragmentation import fragment_molecule
import numpy as np
import pandas as pd
import torch
import pickle
import pathlib
from feos.si import *
from feos_torch import PcSaftPure

class _model_:
    def __init__(self, chem_id, group_info, phys_data=None, normalize=None):
        # Inputs:
        self.data = {"substances":None, "datasets":{}, "dataset_info":{}}

        self.groups = {
            "info":{"fit" : None, "idx" : {}, "known" : group_info["known_groups"], "scaling_factors" : group_info["scaling_factors"]}, 
            "initvals":None, 
            "lobnds":None
            }
        self.__fill_groups_dict__(group_info)

        self.fragments = {}
        for mol in chem_id.index:
            try:
                frag, unitvectors = fragment_molecule(chem_id.loc[(mol, "isomeric_smiles")], group_info["smarts"], group_info["bonds"] if "bonds" in group_info else None)
                self.fragments[mol] = {"groupcounts" : {g : sum(1 for g_ in frag[0] if g_ == g) for g in frag[0]}, "unitvectors" : unitvectors}
            except Exception as e:
                print("An exception occured for {}, {}: {} --> Excluded substance from dataset".format(mol, chem_id.loc[(mol, "isomeric_smiles")], str(e)))

        if "exp_dipole_moment" not in chem_id.columns:
            chem_id["exp_dipole_moment"] = np.nan
        self.data["substances"] = chem_id[["isomeric_smiles","mw","exp_dipole_moment"]].loc[chem_id.index.unique().intersection(self.fragments.keys())].copy(deep=True)
        self.__fill_data_dict__(phys_data, normalize) # Specific for each model, needs to be implemented in subclasses

        # Outputs:
        self.opt_params = {"fit":None, "loocv":{}}
        self.solver_out = {"fit":None, "loocv":{}}
        self.results = {
            "fit" : {"parameters" : None, "stats" : None, "property" : None, "group_parameters" : None},
            "loocv" : {"parameters" : None, "stats" : None, "property" : None}
        }
    
    def __fill_groups_dict__(self, group_info):
        # Separate initial values for bonds and groups
        fit_bonds_initval = {}
        fit_groups_initval = {}
        for group, initval in group_info["initvals"].items():
            if isinstance(initval, float):
                fit_bonds_initval.update({group:{"mu":initval}})
            elif isinstance(initval, dict):
                fit_groups_initval.update({group:initval})
        fit_bonds_initval = pd.DataFrame.from_dict(fit_bonds_initval).transpose()
        fit_groups_initval = pd.DataFrame.from_dict(fit_groups_initval).transpose()
        ## Scale variables for solver
        for par in fit_bonds_initval.columns.intersection(group_info["scaling_factors"].keys()).tolist():
            fit_bonds_initval[par] /= group_info["scaling_factors"][par]
        for par in fit_groups_initval.columns.intersection(group_info["scaling_factors"].keys()).tolist():
            fit_groups_initval[par] /= group_info["scaling_factors"][par]

        # Array with all fitted groups
        self.groups["info"]["fit"] = fit_bonds_initval.index.tolist() + fit_groups_initval.index.tolist()

        # Save information on indices of groups in initvals and lobnds vectors
        self.groups["info"]["idx"] = {group : i for i, group in enumerate(fit_bonds_initval.index.tolist())}
        offset = len(self.groups["info"]["idx"])
        columns = fit_groups_initval.columns.to_list()
        self.groups["info"]["idx"].update({group : {param : offset + i*len(columns) + j for j, param in enumerate(columns)} for i, group in enumerate(fit_groups_initval.index.tolist())})
        
        # Set initial values
        self.groups["initvals"]  = np.concatenate((fit_bonds_initval.values.flatten(), fit_groups_initval.values.flatten()))
        
        # Set lower bounds
        self.groups["lobnds"] = np.zeros(len(self.groups["initvals"]))
        # Check if second order groups (labeled with "SO:" at start of the group) are present, then set -infinity as lower bounds for these groups
        so_group_indices = []
        for group, idx in self.groups["info"]["idx"].items():
            if "SO:" in group:
                so_group_indices.extend(idx.values())
        if any(so_group_indices):
            self.groups["lobnds"][so_group_indices] = -np.inf

    def statistics(self, predictions, targets):
        diff = predictions - targets
        rel_diff = diff / targets
        # average absolute deviations
        aad = np.abs(diff).mean()
        # median absolute deviations
        mad = np.abs(diff).median()
        # average absolute relative deviations
        aard = np.abs(rel_diff).mean()
        # median absolute relative deviations
        median_ard = np.abs(rel_diff).median()

        return aad.data.item(), mad.data.item(), aard.data.item(), median_ard.data.item()

    def save(self, path2out, filename):
        with open("{}/{}.pkl".format(path2out,filename), "wb") as f:
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)
    
class dipole(_model_):
    def __fill_data_dict__(self, *args):
        self.data["dataset_info"] = {
            "idx" : pd.DataFrame(columns=["mu","all"], index=self.data["substances"].index),
            "n_data" : pd.DataFrame(columns=["mu","all"], index=self.data["substances"].index)
            }
        self.data["dataset_info"]["n_data"].loc[self.data["substances"][~pd.isna(self.data["substances"]["exp_dipole_moment"])].index, "mu"] = 1
        self.data["dataset_info"]["n_data"].loc[self.data["substances"][pd.isna(self.data["substances"]["exp_dipole_moment"])].index, "mu"] = 0
        self.data["dataset_info"]["n_data"]["all"] = self.data["dataset_info"]["n_data"]["mu"]

        self.data["dataset_info"]["idx"]["mu"] = self.data["dataset_info"]["idx"].apply(lambda row: range(list(self.data["substances"].index).index(row.name), list(self.data["substances"].index).index(row.name)+1), axis=1)
        self.data["dataset_info"]["idx"]["all"] = self.data["dataset_info"]["idx"]["mu"]

        self.data["datasets"] = {
            "mu" : {"target" : torch.tensor(self.data["substances"]["exp_dipole_moment"].values, dtype=torch.float64)}
            }
    
    def __get_result_dataframes__(self, params):
        property_df = pd.DataFrame(
            columns=["exp","pred","diff"],
            index=list(self.data["substances"].index)
            )
        
        for mol in self.data["substances"].index:

            dipoles = self.predict(params if not isinstance(params,dict) else torch.tensor(params[mol], dtype=torch.float64), [mol])
            if dipoles.nelement()>1:
                mu = dipoles[0].data.item()
            else:
                mu = dipoles.data.item()

            property_df.loc[mol] = pd.Series({
                "exp" : self.data["substances"].loc[mol, "exp_dipole_moment"],
                "pred" : mu,
                "diff" : (self.data["substances"].loc[mol, "exp_dipole_moment"] - mu)

            })

        aad, mad, aard, median_ard = self.statistics(torch.tensor(property_df[~pd.isna(property_df["exp"])]["pred"].astype(np.float64).values, dtype=torch.float64), torch.tensor(property_df[~pd.isna(property_df["exp"])]["exp"].astype(np.float64).values, dtype=torch.float64))
        stats_df = pd.DataFrame({"aad" : aad, "mad" : mad, "aard" : aard, "median_ard" : median_ard}, columns=["aad","mad","aard","median_ard"], index=["all"])

        return property_df, stats_df

    def sum_rules_dipole(self, params, mol):
        scale_mu = self.groups["info"]["scaling_factors"]["mu"] if "mu" in self.groups["info"]["scaling_factors"] else 1.0
        mu_avg = 0.0
        for conf in self.fragments[mol]["unitvectors"]:
            mu_conf = torch.zeros(3, dtype=torch.float64)
            for bond in self.fragments[mol]["unitvectors"][conf]:
                mu_conf += (
                    params[self.groups["info"]["idx"][bond]] * scale_mu 
                    if bond in self.groups["info"]["fit"] 
                    else self.groups["info"]["known"][bond]
                    ) * torch.tensor(self.fragments[mol]["unitvectors"][conf][bond])
            mu_conf = torch.linalg.norm(mu_conf)
            mu_avg += mu_conf
        return mu_avg / len(self.fragments[mol]["unitvectors"])
    
    def predict(self, params, substances):
        mu_pred = torch.tensor([], dtype=torch.float64)
        for mol in substances:
            mu_pred = torch.cat((
                mu_pred, 
                self.sum_rules_dipole(params, mol).reshape(1)
                ))
        return mu_pred

    def cost(self, params, substances):
        substances = self.data["substances"].loc[substances][~pd.isna(self.data["substances"].loc[substances,"exp_dipole_moment"])].index.to_list()
        costs = self.data["datasets"]["mu"]["target"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances, "all"].values))] - self.predict(params, substances)
        return torch.sum(torch.square(costs))/len(costs)
    
    def get_fit_results(self):
        self.results["fit"]["property"], self.results["fit"]["stats"] = self.__get_result_dataframes__(torch.tensor(self.opt_params["fit"], dtype=torch.float64))
        self.results["fit"]["group_parameters"] = pd.concat([
            pd.DataFrame.from_dict({
                k: {"mu": self.opt_params["fit"][v] * self.groups["info"]["scaling_factors"]["mu"]} 
                for k, v in self.groups["info"]["idx"].items()
            }).T.sort_values(by="mu"),
            pd.DataFrame.from_dict({
                k:{"mu":v} if not isinstance(v,dict) else np.nan 
                for k, v in self.groups["info"]["known"].items()
            }).T.sort_values(by="mu")
            ]).dropna()
        
    def get_loocv_results(self):        
        self.results["loocv"]["property"], self.results["loocv"]["stats"] = self.__get_result_dataframes__(self.opt_params["loocv"])

class pcsaftGC(_model_):
    def __fill_data_dict__(self, phys_data, normalize):
        vap_data = phys_data[(phys_data["property"]=="pressure") & (phys_data["phase"]!='Critical Point')]
        liqden_data = phys_data[(phys_data["property"]=="density") & (phys_data["phase"]=="Liquid")]
        vleliqden_data = phys_data[(phys_data["property"]=="density") & (phys_data["phase"]=="Liquid (VLE)")]

        # Dataset info
        self.data["dataset_info"] = {
            "idx" : pd.DataFrame(columns=["vp","rho_liq","rho_vle","all"], index=self.data["substances"].index),
            "n_data" : pd.DataFrame(columns=["vp","rho_liq","rho_vle","all"], index=self.data["substances"].index)
            }
        self.data["dataset_info"]["n_data"]["vp"] = self.data["dataset_info"]["n_data"].apply(lambda row: sum(vap_data.index.values==row.name), axis=1)
        self.data["dataset_info"]["n_data"]["rho_liq"] = self.data["dataset_info"]["n_data"].apply(lambda row: sum(liqden_data.index.values==row.name), axis=1)
        self.data["dataset_info"]["n_data"]["rho_vle"] = self.data["dataset_info"]["n_data"].apply(lambda row: sum(vleliqden_data.index.values==row.name), axis=1)
        self.data["dataset_info"]["n_data"]["all"] = self.data["dataset_info"]["n_data"].apply(lambda row: sum([row["vp"],row["rho_liq"],row["rho_vle"]]), axis=1)
        ranges = [
            range(0,0),
            range(0,0),
            range(0,0)
            ]
        for mol in self.data["dataset_info"]["n_data"].index:
            ranges[0] = range(ranges[0].stop, ranges[0].stop + self.data["dataset_info"]["n_data"].loc[mol, "vp"])
            ranges[1] = range(ranges[1].stop, ranges[1].stop + self.data["dataset_info"]["n_data"].loc[mol, "rho_liq"])
            ranges[2] = range(ranges[2].stop, ranges[2].stop + self.data["dataset_info"]["n_data"].loc[mol, "rho_vle"])
            self.data["dataset_info"]["idx"].loc[mol, "vp"] = ranges[0]
            self.data["dataset_info"]["idx"].loc[mol, "rho_liq"] = ranges[1]
            self.data["dataset_info"]["idx"].loc[mol, "rho_vle"] = ranges[2]
            self.data["dataset_info"]["idx"].loc[mol, "all"] = np.concatenate((ranges[0], np.array(ranges[1])+self.data["dataset_info"]["n_data"]["vp"].sum(), np.array(ranges[2])+self.data["dataset_info"]["n_data"]["vp"].sum()+self.data["dataset_info"]["n_data"]["rho_liq"].sum()))

        # Datasets
        self.data["datasets"] = {
            "vp" : {"temperature" : torch.tensor([], dtype=torch.float64), "target" : torch.tensor([], dtype=torch.float64), "normalize" : torch.tensor([], dtype=torch.float64), "datapoint_id" : np.array([])},
            "rho_liq" : {"temperature" : torch.tensor([], dtype=torch.float64), "pressure" : torch.tensor([], dtype=torch.float64), "target" : torch.tensor([], dtype=torch.float64), "normalize" : torch.tensor([], dtype=torch.float64), "datapoint_id" : np.array([])},
            "rho_vle" : {"temperature" : torch.tensor([], dtype=torch.float64), "target" : torch.tensor([], dtype=torch.float64), "normalize" : torch.tensor([], dtype=torch.float64), "datapoint_id" : np.array([])},
            }
        for mol in self.data["substances"].index:
            # Create datasets for FeOs torch
            # Vap
            self.data["datasets"]["vp"]["temperature"] = torch.cat((self.data["datasets"]["vp"]["temperature"], torch.tensor(vap_data.loc[(mol,"temperature")].values * KELVIN/KELVIN, dtype=torch.float64)))
            self.data["datasets"]["vp"]["target"] = torch.cat((self.data["datasets"]["vp"]["target"], torch.tensor(vap_data.loc[(mol,"value")].values * PASCAL/PASCAL, dtype=torch.float64)))
            if normalize!="none":
                self.data["datasets"]["vp"]["normalize"] = torch.cat((self.data["datasets"]["vp"]["normalize"], torch.tensor([self.data["dataset_info"]["n_data"].loc[mol,"vp"]] * self.data["dataset_info"]["n_data"].loc[mol,"vp"], dtype=torch.float64)))
            self.data["datasets"]["vp"]["datapoint_id"] = np.concatenate((self.data["datasets"]["vp"]["datapoint_id"], np.array(vap_data.loc[(mol,"datapoint_id")].values)))
            
            # Liq den
            if mol in liqden_data.index:
                if isinstance(liqden_data.loc[(mol,"value")], np.float64):
                    T = torch.tensor([liqden_data.loc[(mol,"temperature")] * KELVIN/KELVIN], dtype=torch.float64)
                    p = torch.tensor([liqden_data.loc[(mol,"pressure")] * PASCAL/PASCAL], dtype=torch.float64)
                    rho = torch.tensor([liqden_data.loc[(mol,"value")] * (KILOGRAM/METER**3) / (KILO * MOL/METER**3) / (self.data["substances"].loc[(mol,"mw")] * GRAM / MOL)], dtype=torch.float64)
                    datapoint_ids = np.array([liqden_data.loc[(mol,"datapoint_id")]])
                else:
                    T = torch.tensor(liqden_data.loc[(mol,"temperature")].values * KELVIN/KELVIN, dtype=torch.float64)
                    p = torch.tensor(liqden_data.loc[(mol,"pressure")].values * PASCAL/PASCAL, dtype=torch.float64)
                    rho = torch.tensor(liqden_data.loc[(mol,"value")].values * (KILOGRAM/METER**3) / (KILO * MOL/METER**3) / (self.data["substances"].loc[(mol,"mw")] * GRAM / MOL), dtype=torch.float64)
                    datapoint_ids = np.array(liqden_data.loc[(mol,"datapoint_id")].values)
                self.data["datasets"]["rho_liq"]["temperature"] = torch.cat((self.data["datasets"]["rho_liq"]["temperature"], T))
                self.data["datasets"]["rho_liq"]["pressure"] = torch.cat((self.data["datasets"]["rho_liq"]["pressure"], p))
                self.data["datasets"]["rho_liq"]["target"] = torch.cat((self.data["datasets"]["rho_liq"]["target"], rho))
                if normalize!="none":
                    self.data["datasets"]["rho_liq"]["normalize"] = torch.cat((self.data["datasets"]["rho_liq"]["normalize"], torch.tensor([self.data["dataset_info"]["n_data"].loc[mol,"rho_liq"]] * self.data["dataset_info"]["n_data"].loc[mol,"rho_liq"], dtype=torch.float64)))
                self.data["datasets"]["rho_liq"]["datapoint_id"] = np.concatenate((self.data["datasets"]["rho_liq"]["datapoint_id"], datapoint_ids))

            # VLE liq den
            if mol in vleliqden_data.index:
                if isinstance(vleliqden_data.loc[(mol,"value")], np.float64):
                    T = torch.tensor([vleliqden_data.loc[(mol,"temperature")] * KELVIN/KELVIN], dtype=torch.float64)
                    rho = torch.tensor([vleliqden_data.loc[(mol,"value")] * (KILOGRAM/METER**3) / (KILO * MOL/METER**3) / (self.data["substances"].loc[(mol,"mw")] * GRAM / MOL)], dtype=torch.float64)
                    datapoint_ids = np.array([vleliqden_data.loc[(mol,"datapoint_id")]])
                else:
                    T = torch.tensor(vleliqden_data.loc[(mol,"temperature")].values * KELVIN/KELVIN, dtype=torch.float64)
                    rho = torch.tensor(vleliqden_data.loc[(mol,"value")].values * (KILOGRAM/METER**3) / (KILO * MOL/METER**3) / (self.data["substances"].loc[(mol,"mw")] * GRAM / MOL), dtype=torch.float64)
                    datapoint_ids = np.array(vleliqden_data.loc[(mol,"datapoint_id")].values)
                self.data["datasets"]["rho_vle"]["temperature"] = torch.cat((self.data["datasets"]["rho_vle"]["temperature"], T))
                self.data["datasets"]["rho_vle"]["target"] = torch.cat((self.data["datasets"]["rho_vle"]["target"], rho))
                if normalize!="none":    
                    self.data["datasets"]["rho_vle"]["normalize"] = torch.cat((self.data["datasets"]["rho_vle"]["normalize"], torch.tensor([self.data["dataset_info"]["n_data"].loc[mol,"rho_vle"]] * self.data["dataset_info"]["n_data"].loc[mol,"rho_vle"], dtype=torch.float64)))
                self.data["datasets"]["rho_vle"]["datapoint_id"] = np.concatenate((self.data["datasets"]["rho_vle"]["datapoint_id"], datapoint_ids))
        
        if normalize=="sqrt":
            self.data["datasets"]["vp"]["normalize"] = self.data["datasets"]["vp"]["normalize"].sqrt()
            self.data["datasets"]["rho_liq"]["normalize"] = self.data["datasets"]["rho_liq"]["normalize"].sqrt()
            self.data["datasets"]["rho_vle"]["normalize"] = self.data["datasets"]["rho_vle"]["normalize"].sqrt()
        elif normalize=="none":
            self.data["datasets"]["vp"]["normalize"] = torch.ones_like(self.data["datasets"]["vp"]["target"])
            self.data["datasets"]["rho_liq"]["normalize"] = torch.ones_like(self.data["datasets"]["rho_liq"]["target"])
            self.data["datasets"]["rho_vle"]["normalize"] = torch.ones_like(self.data["datasets"]["rho_vle"]["target"])
    
    def __get_result_dataframes__(self, params):
        parameters_df = pd.DataFrame(columns=["m","sigma","epsilon","mu","kappa_ab","epsilon_ab","na","nb"], index = self.data["substances"].index)
        property_df = {
            "vp" : pd.DataFrame(columns=["mol_id","T","exp","pred","rel_diff","datapoint_id"]),
            "rho_liq" : pd.DataFrame(columns=["mol_id","T","p","exp","pred","rel_diff","datapoint_id"]),
            "rho_vle" : pd.DataFrame(columns=["mol_id","T","exp","pred","rel_diff","datapoint_id"])
            }
        stats_df = pd.DataFrame(
            columns=[
                ("aad","vp"),("aad","rho_liq"),("aad","rho_vle"),
                ("mad","vp"),("mad","rho_liq"),("mad","rho_vle"),
                ("aard","vp"),("aard","rho_liq"),("aard","rho_vle"),
                ("median_ard","vp"),("median_ard","rho_liq"),("median_ard","rho_vle"),
                ], 
            index=list(self.data["substances"].index) + ["all","average_over_mols","median_over_mols"]
            )
    
        for mol in self.data["substances"].index:

            m, sigma, epsilon, mu, kappa_ab, epsilon_ab, na, nb = self.sum_rules_pcpsaft(params if not isinstance(params,dict) else torch.tensor(params[mol], dtype=torch.float64), mol)
            parameters_df.loc[mol] = pd.Series({
                "m" : m.data.item(),
                "sigma" : sigma.data.item(),
                "epsilon" : epsilon.data.item(),
                "mu" : mu.data.item(),
                "kappa_ab" : kappa_ab.data.item(),
                "epsilon_ab" : epsilon_ab.data.item(), 
                "na" : na.data.item(), 
                "nb" : nb.data.item()
            })

            vp_nan, vp_pred, rho_liq_nan, rho_liq_pred, rho_vle_nan, rho_vle_pred = self.predict(params if not isinstance(params,dict) else torch.tensor(params[mol], dtype=torch.float64), [mol])
            idx_vp, idx_rho_liq, idx_rho_vle = np.array(self.data["dataset_info"]["idx"].loc[mol,"vp"])[~vp_nan], np.array(self.data["dataset_info"]["idx"].loc[mol,"rho_liq"])[~rho_liq_nan if len(rho_liq_nan)>0 else []], np.array(self.data["dataset_info"]["idx"].loc[mol,"rho_vle"])[~rho_vle_nan if len(rho_vle_nan)>0 else []]
            vp_exp_mol, rho_liq_exp_mol, rho_vle_exp_mol = self.data["datasets"]["vp"]["target"][idx_vp], self.data["datasets"]["rho_liq"]["target"][idx_rho_liq], self.data["datasets"]["rho_vle"]["target"][idx_rho_vle]

            property_df["vp"] = pd.concat([
                property_df["vp"],
                pd.DataFrame({
                    "mol_id" : [mol] * len(idx_vp),
                    "T" : self.data["datasets"]["vp"]["temperature"][idx_vp].numpy(),
                    "exp" : vp_exp_mol.numpy(),
                    "pred" : vp_pred.numpy(),
                    "rel_diff" : (vp_exp_mol.numpy() - vp_pred.numpy()) / vp_exp_mol.numpy(),
                    "datapoint_id" : self.data["datasets"]["vp"]["datapoint_id"][idx_vp]
                    })
            ])

            property_df["rho_liq"] = pd.concat([
                property_df["rho_liq"],
                pd.DataFrame({
                    "mol_id" : [mol] * len(idx_rho_liq),
                    "T" : self.data["datasets"]["rho_liq"]["temperature"][idx_rho_liq].numpy(),
                    "p" : self.data["datasets"]["rho_liq"]["pressure"][idx_rho_liq].numpy(),
                    "exp" : rho_liq_exp_mol.numpy(),
                    "pred" : rho_liq_pred.numpy(),
                    "rel_diff" : (rho_liq_exp_mol.numpy() - rho_liq_pred.numpy()) / rho_liq_exp_mol.numpy(),
                    "datapoint_id" : self.data["datasets"]["rho_liq"]["datapoint_id"][idx_rho_liq] if idx_rho_liq.any() else np.array([])
                })
            ])
            
            property_df["rho_vle"] = pd.concat([
                property_df["rho_vle"],
                pd.DataFrame({
                    "mol_id" : [mol] * len(idx_rho_vle),
                    "T" : self.data["datasets"]["rho_vle"]["temperature"][idx_rho_vle].numpy(),
                    "exp" : rho_vle_exp_mol.numpy(),
                    "pred" : rho_vle_pred.numpy(),
                    "rel_diff" : (rho_vle_exp_mol.numpy() - rho_vle_pred.numpy()) / rho_vle_exp_mol.numpy(),
                    "datapoint_id" : self.data["datasets"]["rho_vle"]["datapoint_id"][idx_rho_vle] if idx_rho_vle.any() else np.array([])
                })
            ])

            stats_vp, stats_rholiq, stats_rhovle = self.statistics(vp_pred, vp_exp_mol), self.statistics(rho_liq_pred, rho_liq_exp_mol), self.statistics(rho_vle_pred, rho_vle_exp_mol)
            stats_df.loc[mol] = pd.Series({
                ("aad","vp") : stats_vp[0], ("aad","rho_liq") : stats_rholiq[0], ("aad","rho_vle") : stats_rhovle[0],
                ("mad","vp") : stats_vp[1], ("mad","rho_liq") : stats_rholiq[1], ("mad","rho_vle") : stats_rhovle[1],
                ("aard","vp") : stats_vp[2], ("aard","rho_liq") : stats_rholiq[2], ("aard","rho_vle") : stats_rhovle[2],
                ("median_ard","vp") : stats_vp[3], ("median_ard","rho_liq") : stats_rholiq[3], ("median_ard","rho_vle") : stats_rhovle[3]
                })
            
        property_df["vp"] = property_df["vp"].set_index("mol_id")
        property_df["rho_liq"] = property_df["rho_liq"].set_index("mol_id")
        property_df["rho_vle"] = property_df["rho_vle"].set_index("mol_id")

        stats_vp = self.statistics(torch.tensor(property_df["vp"]["pred"].values.astype(np.float64), dtype=torch.float64), torch.tensor(property_df["vp"]["exp"].values.astype(np.float64), dtype=torch.float64))
        stats_rholiq = self.statistics(torch.tensor(property_df["rho_liq"]["pred"].values.astype(np.float64), dtype=torch.float64), torch.tensor(property_df["rho_liq"]["exp"].values.astype(np.float64), dtype=torch.float64))
        stats_rhovle = self.statistics(torch.tensor(property_df["rho_vle"]["pred"].values.astype(np.float64), dtype=torch.float64), torch.tensor(property_df["rho_vle"]["exp"].values.astype(np.float64), dtype=torch.float64))
        stats_df.loc["all"] = pd.Series({
            ("aad","vp") : stats_vp[0], ("aad","rho_liq") : stats_rholiq[0], ("aad","rho_vle") : stats_rhovle[0],
            ("mad","vp") : stats_vp[1], ("mad","rho_liq") : stats_rholiq[1], ("mad","rho_vle") : stats_rhovle[1],
            ("aard","vp") : stats_vp[2], ("aard","rho_liq") : stats_rholiq[2], ("aard","rho_vle") : stats_rhovle[2],
            ("median_ard","vp") : stats_vp[3], ("median_ard","rho_liq") : stats_rholiq[3], ("median_ard","rho_vle") : stats_rhovle[3]
            })
        
        stats_df.loc["average_over_mols"] = stats_df.iloc[:-3].mean()
        stats_df.loc["median_over_mols"] = stats_df.iloc[:-3].median()

        return parameters_df, property_df, stats_df
    
    def sum_rules_pcpsaft(self, params, mol):
        # Sum rule for m from Vijande et al., 2004; Sauer et al., 2014
        m = sum(
            (
                params[self.groups["info"]["idx"][group]["m"]] * self.groups["info"]["scaling_factors"]["m"]
                if group in self.groups["info"]["fit"] 
                else self.groups["info"]["known"][group]["m"]
                ) * self.fragments[mol]["groupcounts"][group] 
            for group in self.fragments[mol]["groupcounts"].keys()
            )
        
        # Sum rule for sigma from Vijande et al., 2004; Sauer et al., 2014 
        sigma = (sum(
            (
                ((params[self.groups["info"]["idx"][group]["m"]] * self.groups["info"]["scaling_factors"]["m"]) * (params[self.groups["info"]["idx"][group]["sigma"]] * self.groups["info"]["scaling_factors"]["sigma"])**3) 
                if group in self.groups["info"]["fit"] 
                else (self.groups["info"]["known"][group]["m"] * self.groups["info"]["known"][group]["sigma"]**3)
                ) * self.fragments[mol]["groupcounts"][group] 
            for group in self.fragments[mol]["groupcounts"].keys()
            ) / 
            m
            )**(1/3) 
        
        # Sum rule for epsilon from Vijande et al., 2004; Sauer et al., 2014
        epsilon = (sum(
            (
                ((params[self.groups["info"]["idx"][group]["m"]] * self.groups["info"]["scaling_factors"]["m"]) * (params[self.groups["info"]["idx"][group]["epsilon"]] * self.groups["info"]["scaling_factors"]["epsilon"])) 
                if group in self.groups["info"]["fit"] 
                else (self.groups["info"]["known"][group]["m"] * self.groups["info"]["known"][group]["epsilon"])
                ) * self.fragments[mol]["groupcounts"][group]
            for group in self.fragments[mol]["groupcounts"].keys()
            ) /
            m
            )
        
        # Sum rule for the dipole moment
        mu = self.sum_rules_dipole(params, mol)

        # Sum rule for the association parameters
        kappa_ab, epsilon_ab, na, nb = self.sum_rules_assoc(params, mol)

        return m, sigma, epsilon, mu, kappa_ab, epsilon_ab, na, nb

    def sum_rules_dipole(self, params, mol):
        # returns mu
        return torch.tensor(0, dtype=torch.float64)

    def sum_rules_assoc(self, params, mol):
        # returns kappa_ab, epsilon_ab, na, nb
        return torch.zeros(4, dtype=torch.float64)

    def predict(self, params, substances):
        p_vp = torch.tensor([], dtype=torch.float64)
        p_rholiq = torch.tensor([], dtype=torch.float64)
        p_rhovle = torch.tensor([], dtype=torch.float64)
        for mol in substances:
            m, sigma, epsilon, mu, kappa_ab, epsilon_ab, na, nb = self.sum_rules_pcpsaft(params, mol)

            p = torch.tensor([[.0,.0,.0,.0,.0,.0,.0,.0]] * self.data["dataset_info"]["n_data"].loc[mol,"vp"], dtype=torch.float64)
            p[:,0] = m
            p[:,1] = sigma
            p[:,2] = epsilon
            p[:,3] = mu
            p[:,4] = kappa_ab
            p[:,5] = epsilon_ab 
            p[:,6] = na
            p[:,7] = nb
            p_vp = torch.cat((p_vp, p))

            if self.data["dataset_info"]["n_data"].loc[mol,"rho_liq"] > 0:
                p = torch.tensor([[.0,.0,.0,.0,.0,.0,.0,.0]] * self.data["dataset_info"]["n_data"].loc[mol,"rho_liq"], dtype=torch.float64)
                p[:,0] = m
                p[:,1] = sigma
                p[:,2] = epsilon
                p[:,3] = mu
                p[:,4] = kappa_ab
                p[:,5] = epsilon_ab 
                p[:,6] = na
                p[:,7] = nb
                p_rholiq = torch.cat((p_rholiq, p))

            if self.data["dataset_info"]["n_data"].loc[mol,"rho_vle"] > 0:
                p = torch.tensor([[.0,.0,.0,.0,.0,.0,.0,.0]] * self.data["dataset_info"]["n_data"].loc[mol,"rho_vle"], dtype=torch.float64)
                p[:,0] = m
                p[:,1] = sigma
                p[:,2] = epsilon
                p[:,3] = mu
                p[:,4] = kappa_ab
                p[:,5] = epsilon_ab 
                p[:,6] = na
                p[:,7] = nb
                p_rhovle = torch.cat((p_rhovle, p))
        
        eos = PcSaftPure(p_vp)
        vp_nan, vp_pred = eos.vapor_pressure(self.data["datasets"]["vp"]["temperature"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances,"vp"].values))])
        
        if p_rholiq.nelement()!=0:
            eos = PcSaftPure(p_rholiq)
            rho_liq_nan, rho_liq_pred = eos.liquid_density(self.data["datasets"]["rho_liq"]["temperature"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances,"rho_liq"].values))], self.data["datasets"]["rho_liq"]["pressure"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances,"rho_liq"].values))])
        else:
            rho_liq_nan, rho_liq_pred = np.ones_like(self.data["datasets"]["rho_liq"]["target"], dtype=bool), torch.tensor([], dtype=torch.float64)

        if p_rhovle.nelement()!=0:
            eos = PcSaftPure(p_rhovle)
            rho_vle_nan, rho_vle_pred = eos.equilibrium_liquid_density(self.data["datasets"]["rho_vle"]["temperature"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances,"rho_vle"].values))])
        else:
            rho_vle_nan, rho_vle_pred = np.ones_like(self.data["datasets"]["rho_vle"]["target"], dtype=bool), torch.tensor([], dtype=torch.float64)

        return vp_nan, vp_pred, rho_liq_nan, rho_liq_pred, rho_vle_nan, rho_vle_pred
    
    def cost(self, params, substances):
        vp_nan, vp_pred, rho_liq_nan, rho_liq_pred, rho_vle_nan, rho_vle_pred = self.predict(params, substances)
        
        vp = torch.log(vp_pred / self.data["datasets"]["vp"]["target"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances,"vp"].values))][~vp_nan]).square() / self.data["datasets"]["vp"]["normalize"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances,"vp"].values))][~vp_nan]
        rho_liq = torch.log(rho_liq_pred / self.data["datasets"]["rho_liq"]["target"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances,"rho_liq"].values))][~rho_liq_nan]).square() / self.data["datasets"]["rho_liq"]["normalize"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances,"rho_liq"].values))][~rho_liq_nan]
        rho_vle = torch.log(rho_vle_pred / self.data["datasets"]["rho_vle"]["target"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances,"rho_vle"].values))][~rho_vle_nan]).square() / self.data["datasets"]["rho_vle"]["normalize"][np.sort(np.concatenate(self.data["dataset_info"]["idx"].loc[substances,"rho_vle"].values))][~rho_vle_nan]

        return ( 3*vp.sum()  + 2*rho_vle.sum() + 2*rho_liq.sum() ) / ( len(vp) + len(rho_vle) + len(rho_liq) )
    
    def get_fit_results(self):
        self.results["fit"]["parameters"], self.results["fit"]["property"], self.results["fit"]["stats"] = self.__get_result_dataframes__(torch.tensor(self.opt_params["fit"], dtype=torch.float64))
        self.results["fit"]["group_parameters"] = pd.concat([
            pd.DataFrame.from_dict({ 
                k : {
                    "m": self.opt_params["fit"][v["m"]] * self.groups["info"]["scaling_factors"]["m"],
                    "sigma": self.opt_params["fit"][v["sigma"]] * self.groups["info"]["scaling_factors"]["sigma"],
                    "epsilon": self.opt_params["fit"][v["epsilon"]] * self.groups["info"]["scaling_factors"]["epsilon"]
                    } if isinstance(v,dict) 
                    else {
                        "mu": self.opt_params["fit"][v] * self.groups["info"]["scaling_factors"]["mu"]
                    } 
                for k, v in self.groups["info"]["idx"].items()
                }).T.sort_values(by="m"),
            pd.DataFrame.from_dict({
                k : v if isinstance(v,dict) else {"mu":v} 
                for k, v in self.groups["info"]["known"].items()
                }).T.sort_values(by="m")
            ]).reindex(columns=["m","sigma","epsilon","mu"])
    
    def get_loocv_results(self):        
        self.results["loocv"]["parameters"], self.results["loocv"]["property"], self.results["loocv"]["stats"] = self.__get_result_dataframes__(self.opt_params["loocv"])
        
class muzeroGC(pcsaftGC):
    def __fill_groups_dict__(self, group_info):
        # Exclude bonds from fit
        group_info["initvals"] = {group: initval for group, initval in group_info["initvals"].items() if not isinstance(initval, float)}
        super().__fill_groups_dict__(group_info)

class vectorGC(pcsaftGC, dipole):
    def sum_rules_dipole(self, params, mol):
        return dipole.sum_rules_dipole(self, params, mol)