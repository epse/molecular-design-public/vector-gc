import scipy, torch, tqdm

try:
    from knitro import *
except:
    print("Knitro could not be imported and is not available. Use the implemented scipy solver.")

class solver:
    def __init__(self, show_progress):
        self.show = show_progress
    
    def fit(self, model, initvals=None, lobnds=None):            
        res = self.solve(model, model.data["substances"].index, initvals if initvals is not None else model.groups["initvals"], lobnds if lobnds is not None else model.groups["lobnds"])
        model.opt_params["fit"] = res[2]
        model.solver_out["fit"] = res
        return res
    
    def loocv(self, model, initvals=None, lobnds=None):
        results = {}
        progress = tqdm.tqdm(model.data["substances"].index)
        for mol in progress:
            progress.set_postfix_str(mol)

            # Check isomers
            if "/" in model.data["substances"].loc[mol, "isomeric_smiles"]:
                tmp = model.data["substances"].loc[mol, "isomeric_smiles"]
                # Generate string of second isomer
                if tmp.count("/")==2:
                    idx = tmp.index("/", tmp.index("/")+1)
                    tmp = list(tmp)
                    tmp[idx] = "\\"
                    tmp = "".join(tmp)
                elif tmp.count("/")==1:
                    idx = tmp.index("\\")
                    tmp = list(tmp)
                    tmp[idx] = "/"
                    tmp = "".join(tmp)
                # Check if second isomer is in the data set
                if tmp in model.data["substances"]["isomeric_smiles"].tolist():
                    mol_j = model.data["substances"][model.data["substances"]["isomeric_smiles"]==tmp].index[0]
                    if mol_j in results:
                        results[mol] = results[mol_j]
                        model.opt_params["loocv"][mol] = model.opt_params["loocv"][mol_j]
                        model.solver_out["loocv"][mol] = model.solver_out["loocv"][mol_j]
                    else:
                        results[mol] = self.solve(model, model.data["substances"].index.drop([mol,mol_j]), initvals if initvals is not None else model.groups["initvals"], lobnds if lobnds is not None else model.groups["lobnds"])
                        model.opt_params["loocv"][mol] = results[mol][2]
                        model.solver_out["loocv"][mol] = results[mol]
                    continue

            results[mol] = self.solve(model, model.data["substances"].index.drop(mol), initvals if initvals is not None else model.groups["initvals"], lobnds if lobnds is not None else model.groups["lobnds"])
            model.opt_params["loocv"][mol] = results[mol][2]
            model.solver_out["loocv"][mol] = results[mol]
        return results

class scipy_solver(solver):
    def solve(self, model, substances, initvals, lobnds):
        def fun(x, substances):            
            x_torch = torch.tensor(x, requires_grad=True, dtype=torch.float64)
            loss = model.cost(x_torch, substances)

            return loss.data.item(), torch.autograd.grad(loss, x_torch, create_graph=True, retain_graph=True)[0].detach().numpy()
    
        def callback(xk):
            obj = model.cost(torch.tensor(xk, dtype=torch.float64), model.data["substances"].index).data.item()
            print(f"Iteration {callback.nfev}: x = {xk}, fobj = {obj}")
            callback.nfev += 1
        
        callback.nfev = 0
        
        if self.show:
            res = scipy.optimize.minimize(fun=fun, 
                                        x0=initvals,
                                        args=(substances),
                                        jac=True,
                                        bounds=[(lower, None) for lower in lobnds],
                                        callback=callback
                                        )
        else:
            res = scipy.optimize.minimize(fun=fun, 
                                        x0=initvals,
                                        args=(substances),
                                        jac=True,
                                        bounds=[(lower, None) for lower in lobnds]
                                        )
        
        return res.status, res.fun, res.x, res
    
class knitro_solver(solver):
    def solve(self, model, substances, initvals, lobnds):
        def callbackEvalFCGA(kc, cb, evalRequest, evalResult, userParams):
                if evalRequest.type != KN_RC_EVALFCGA:
                    print("*** callbackEvalFCGA incorrectly called with eval type %d" %
                        evalRequest.type)
                    return -1
                
                x_torch = torch.tensor(evalRequest.x, requires_grad=True, dtype=torch.float64)
                loss = model.cost(x_torch, substances)
                
                evalResult.obj  = loss.data.item()
                evalResult.objGrad = torch.autograd.grad(loss, x_torch, create_graph=True, retain_graph=True)[0].detach().numpy()

                return 0

        # Create Knitro instance
        try:
            kc = KN_new()
        except:
            print("Failed to find a valid license.")
            quit()

        # Add variables
        KN_add_vars(kc, len(initvals))

        # Set bounds
        KN_set_var_lobnds(kc=kc, xLoBnds=lobnds)

        # Set initial values
        KN_set_var_primal_init_values(kc=kc, xInitVals=initvals)

        # Add callback
        cb = KN_add_eval_callback(kc, evalObj=True, funcCallback=callbackEvalFCGA)
        KN_set_cb_grad(kc, cb, objGradIndexVars=KN_DENSE)

        # Set knitro options
        if self.show:
            KN_set_int_param(kc, KN_PARAM_OUTLEV, 6)
        else:
            KN_set_int_param(kc, KN_PARAM_OUTLEV, 0)
        KN_set_int_param(kc, KN_PARAM_OUTMODE, 0)
        KN_set_int_param(kc, KN_PARAM_EVAL_FCGA, KN_EVAL_FCGA_YES)
        #KN_set_double_param(kc, KN_PARAM_FINDIFF_RELSTEPSIZE, 1e-3)
        #KN_set_int_param(kc, KN_PARAM_MULTISTART, KN_MULTISTART_YES)
        #KN_set_int_param(kc, KN_PARAM_NUMTHREADS, 1)
        #KN_set_int_param(kc, KN_PARAM_MS_MAXSOLVES, 5)

        # Solve instance
        KN_solve(kc)

        # Read solution
        nStatus, objSol, x, _ = KN_get_solution(kc)

        return nStatus, objSol, x