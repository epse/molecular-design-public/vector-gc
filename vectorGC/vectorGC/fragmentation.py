from rdkit.Chem import AllChem as Chem
import numpy as np
import warnings

def customshowwarning(message, category, filename, lineno, file=None, line=None):
    print(f"WARNING: {message}")
warnings.showwarning = customshowwarning

# Smarts
smarts = {
    # NON-POLAR, NON-ASSOCIATING ALIPHATIC FUNCTIONAL GROUPS
    # alkanes -CH3 : no ring, no ethane, no methanol, no methylamine
    "-CH3": "[CH3;!R;!$([CH3][CH3]);!$([CH3][OH]);!$([CH3][NH2])]",
    # alkanes -CH2- : no ring
    "-CH2-": "[$([CX4H2]);!R;!$([CH2]=O)]",
    # alkanes >CH- : no ring
    ">CH-": "[$([CX4H1]);!R;!$([CH2]=O)]",
    # alkanes >C< : no ring
    ">C<": "[$([CX4H0]);!R]",
    # alkenes =CH2 : no ring, no ethylene, no formaldehyde
    "=CH2": "[CX3H2;!R;!$([CH2]=[CH2]);!$([CH2]=O)]",
    # alkenes =CH- : no ring, no aldehyde
    "=CH-": "[CX3H1;!R;!$([CX3H1]=O);!$([CH2]=O)]",
    # alkenes >C= : no ring, no ketone
    ">C=": "[CX3H0;!R;!$([CX3H0]=O);!$([CH2]=O)]",
    # halogenated alkanes >C< : no ring
    "SO:(>C<)_Halo_branch": "[$([CX4H0]([F,Cl,Br,I])([C])([C])[C]);!R]",
    "SO:(>C<)_Halo_middle": "[$([CX4H0]([F,Cl,Br,I])([F,Cl,Br,I])([C])[C]);!R]",
    "SO:(>C<)_Halo_end": "[$([CX4H0]([F,Cl,Br,I])([F,Cl,Br,I])([F,Cl,Br,I])[C]);!R]",
    # halogenated alkanes -CH2- : no ring
    "SO:(-CH2-)_Halo": "[$([CX4H2][F,Cl,Br,I]);!R]",
    # halogenated alkanes >CH- : no ring
    "SO:(>CH-)_Halo_middle": "[$([CX4H1]([F,Cl,Br,I])([C])[C]);!R]",
    "SO:(>CH-)_Halo_end": "[$([CX4H1]([F,Cl,Br,I])([F,Cl,Br,I])[C]);!R]",
    # halogenated alkenes >C= : no ring, no ketone
    "SO:(>C=)_Halo_middle": "[$([CX3H0]([F,Cl,Br,I])([C])=C);!R]",
    "SO:(>C=)_Halo_end": "[$([CX3H0]([F,Cl,Br,I])([F,Cl,Br,I])=C);!R]",
    # halogenated alkenes =CH- : no ring, no aldehyde
    "SO:(=CH-)_Halo": "[$([CX3H1][F,Cl,Br,I]);!R;!$([CX3H1]=O)]",
    #
    # POLAR GROUPS
    # HALOGENS
    # fluorine F : neighbor has to be C
    "F": "[$(F[C])]",
    # chlorine Cl : neighbor has to be C
    "Cl": "[$(Cl[C])]",
    # bromine Br : neighbor has to be C
    "Br": "[$(Br[C])]",
    # iodine I : neighbor has to be C
    "I": "[$(I[C])]",
    # OXYGENATED
    # aldehydes -CH=O : neighbor has to be C
    "CH=O": "[$([CH1][C])]=O",
    # ketones >C=O : both neighbors have to be C, no rings
    ">C=O": "[$([C]([C])([C]));!R]=O",
    # ethers -O- : both neighbors have to be C, no rings, no formates/esters
    "O": "[$([O]([C])[C]);!R;!$([O][C]=O)]",
    # formates -OCH=O, neighbor has to be C
    "-O-CH=O": "[CH1](=O)[$([OH0][C])]",
    # esters -OC(=O)- : both neighbors have to be C, no rings
    "-O-(C=O)-": "[$([CH0][C]);!R](=O)[$([OH0]([C])[C])]"
}

bonds = {
    # CARBON - CARBON bonds with different hybridization
    "Csp2-Csp3" : ['Csp2','Csp3',1.0],

    # CARBON - HYDROGEN
    "C-H" : ['C','H',1.0],
    
    # CARBON - HALOGEN bonds
    # Fluorine
    "F-C" : ['F','C',1.0],
    # Chlorine
    "Cl-C" : ['Cl','C',1.0],
    # Bromine
    "Br-C" : ['Br','C',1.0],
    # Iodine
    "I-C" : ['I','C',1.0],

    # CARBON - OXYGEN bonds
    # Single
    "O-C" : ['O','C',1.0],
    # Double
    "O=C" : ['O','C',2.0],
}

mutually_exclusive = [
    ["F","Cl","Br","I"],
    ["CH=O",">C=O","O","-O-CH=O","-O-(C=O)-"]
]

def fragment_molecule(smiles):
    mol = Chem.MolFromSmiles(Chem.MolToSmiles(Chem.MolFromSmiles(smiles)))
    atoms = mol.GetNumHeavyAtoms()

    # find the location of all fragment using the given smarts
    matches = dict(
        (g, mol.GetSubstructMatches(Chem.MolFromSmarts(smarts[g]))) for g in smarts
    )
    fragments = convert_matches(atoms, matches)
    if all(any(g in fragments.keys() for g in l) for l in mutually_exclusive):
        warnings.warn("Molecule can be fragmented with the defined groups but was not parametrized for the present combination of groups.")

    # get unit vectors of molecule
    unitvectors = get_unitvectors(mol, bonds)
    return fragments, unitvectors

def convert_matches(atoms, matches):
    # check if every atom is captured by exatly one fragment
    identified_atoms = [i for key, l in matches.items() if "SO:" not in key for k in l for i in k]
    unique_atoms = set(identified_atoms)
    if len(unique_atoms) == len(identified_atoms) and len(unique_atoms) == atoms:
        # Translate the atom indices to segment indices (some segments contain more than one atom)
        segment_indices = sorted(
            (sorted(k), group) for group, l in matches.items() for k in l
        )
        segments = [group for _, group in segment_indices]
        segments = {g : sum(1 for g_ in segments if g_ == g) for g in segments}
        return segments
    raise Exception("Molecule cannot be fragmented with the defined groups!")

def get_unitvectors(mol, bonds):
    molh = Chem.AddHs(mol)

    Chem.EmbedMolecule(molh, randomSeed=0xf00d)
    Chem.MMFFOptimizeMolecule(molh)

    unitvectors = {}
    bonds = dict((k,tuple(v)) for k,v in bonds.items())
    bonds_ = dict((v,k) for k,v in bonds.items())

    atoms = {}
    for a in molh.GetAtoms():
        a_idx = a.GetIdx()
        atoms[a_idx] = {"symbol" : a.GetSymbol(), "hybridization" : "", "coordinates" : {0 : molh.GetConformers()[0].GetAtomPosition(a_idx)}}
        if atoms[a_idx]["symbol"] == "C":
            if a.GetHybridization() == Chem.HybridizationType(4):
                atoms[a_idx]["hybridization"] = "sp3"
            elif a.GetHybridization() == Chem.HybridizationType(3):
                atoms[a_idx]["hybridization"] = "sp2"
            elif a.GetHybridization() == Chem.HybridizationType(2):
                atoms[a_idx]["hybridization"] = "sp"

    for b in molh.GetBonds():
        begin_atom = {"idx" : b.GetBeginAtomIdx(), "symbol" : atoms[b.GetBeginAtomIdx()]["symbol"]}
        end_atom = {"idx" : b.GetEndAtomIdx(), "symbol" : atoms[b.GetEndAtomIdx()]["symbol"]}
        if begin_atom["symbol"] == "C" and end_atom["symbol"] == "C":
            begin_atom["symbol"] += atoms[begin_atom["idx"]]["hybridization"]
            end_atom["symbol"] += atoms[end_atom["idx"]]["hybridization"]
        #
        if begin_atom["symbol"] == end_atom["symbol"]:
            continue
        # 
        bond_tuple = [bond_tuple for bond_tuple in bonds_ if (begin_atom["symbol"] in bond_tuple and end_atom["symbol"] in bond_tuple and b.GetBondTypeAsDouble()==bond_tuple[2])]
        if bond_tuple:
            bond_tuple = bond_tuple[0]
            vec = np.array([
            atoms[end_atom["idx"]]["coordinates"][0].x - atoms[begin_atom["idx"]]["coordinates"][0].x,
            atoms[end_atom["idx"]]["coordinates"][0].y - atoms[begin_atom["idx"]]["coordinates"][0].y,
            atoms[end_atom["idx"]]["coordinates"][0].z - atoms[begin_atom["idx"]]["coordinates"][0].z
            ])
            vec /= np.linalg.norm(vec)
            if end_atom["symbol"] == bond_tuple[0]:
                vec *= -1.0
            if bonds_[bond_tuple] not in unitvectors:
                unitvectors[bonds_[bond_tuple]] = vec
            else:
                unitvectors[bonds_[bond_tuple]] += vec
        else:
            Exception("Molecule cannot be fragmented with the defined bonds!")
        
    return unitvectors