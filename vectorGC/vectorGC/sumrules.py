import numpy as np

# Group parameters
group_params = {
  "-CH3":{"m":0.61198, "sigma":3.7202, "epsilon":229.9},
  "-CH2-":{"m":0.45606, "sigma":3.89, "epsilon":239.01},
  ">CH-":{"m":0.14304, "sigma":4.8597, "epsilon":347.64},
  ">C<":{"m":-0.66997, "sigma":-1.7878, "epsilon":107.68},
  "=CH-":{"m":0.56361, "sigma":3.5591, "epsilon":216.69},
  "=CH2":{"m":0.36939, "sigma":4.264, "epsilon":289.49},
  ">C=": {"m":0.86367, "sigma":3.1815, "epsilon":156.31},

  "CH=O":{"m":1.197101835, "sigma":3.2927992827, "epsilon":276.072457704},
  ">C=O":{"m":0.9162169368, "sigma":3.0170859938, "epsilon":304.4360251834},
  "O":{"m":0.762118248, "sigma":2.6156759433, "epsilon":189.5246616811},  
  "-O-CH=O":{"m":1.7323329332, "sigma":2.8665663105, "epsilon":236.1774625206},
  "-O-(C=O)-":{"m":1.6928695878, "sigma":2.635714913, "epsilon":225.8056370659},

  "F":{"m":0.4084776369, "sigma":2.1462756837, "epsilon":214.3161711332},
  "Cl":{"m":0.2434291924, "sigma":4.5934101322, "epsilon":645.9244325184},
  "Br":{"m":0.7084277876, "sigma":3.5825065635, "epsilon":369.4616140702},
  "I":{"m":0.8556016391, "sigma":3.7665616903, "epsilon":393.9827121935},

  "SO:(-CH2-)_Halo":{"m":-0.1057683905, "sigma":1.1506934427, "epsilon":-191.3335337498},
  "SO:(>CH-)_Halo_middle":{"m":0.4486216519, "sigma":3.0323282647, "epsilon":96.2097053957},
  "SO:(>CH-)_Halo_end":{"m":1.9196919855, "sigma":1.926188373, "epsilon":96.69840412},
  "SO:(>C<)_Halo_branch":{"m":0.0219938541, "sigma":3.5191141116, "epsilon":-80.7580218186},
  "SO:(>C<)_Halo_middle":{"m":0.3477776312, "sigma":4.2209056946, "epsilon":-7.532824733},
  "SO:(>C<)_Halo_end":{"m":0.8197395074, "sigma":3.3203271301, "epsilon":21.9601070719},
  "SO:(=CH-)_Halo":{"m":0.1093563657, "sigma":5.0699851865, "epsilon":-109.8439800633},
  "SO:(>C=)_Halo_middle":{"m":0.0834513462, "sigma":-0.1047523671, "epsilon":-343.8788588603},
  "SO:(>C=)_Halo_end":{"m":0.3530199927, "sigma":-0.7360522413, "epsilon":-173.914910487},
  
  "C-H": 0.0,
  "Csp2-Csp3": 0.0,

  "O-C": 0.3444733535,
  "O=C": 2.9832505798,

  "F-C": 2.0712371713,
  "Cl-C": 1.9556362263,
  "Br-C": 1.9359105591,
  "I-C": 1.902653196
}

def sum_rules(groupcounts, unitvectors):
    # Sum rule for m
    m = sum(
        group_params[group]["m"] * groupcount 
        for group, groupcount in groupcounts.items()
        )
        
    # Sum rule for sigma
    sigma = (sum(
            (group_params[group]["m"] * group_params[group]["sigma"]**3) * groupcount 
            for group, groupcount in groupcounts.items()
        ) / m
        )**(1/3) 
        
    # Sum rule for epsilon 
    epsilon = sum(
        (
            group_params[group]["m"] * group_params[group]["epsilon"] * groupcount
            for group, groupcount in groupcounts.items()
        )) / m
        
    # Sum rule for the dipole moment
    mu = np.linalg.norm(
        sum(group_params[bond] * unitvector
            for bond, unitvector in unitvectors.items())
        )

    return m, sigma, epsilon, mu