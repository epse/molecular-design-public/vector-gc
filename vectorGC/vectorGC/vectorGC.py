import numpy as np
from .fragmentation import fragment_molecule, smarts, bonds
from .sumrules import sum_rules, group_params

def from_smiles(smiles, decimals=3):
    # Fragment molecule
    groupcounts, unitvectors = fragment_molecule(smiles)
    # Perform sum rules with parametrized group contributions
    m, sigma, epsilon, mu = sum_rules(groupcounts, unitvectors)

    return np.round(m,decimals), np.round(sigma,decimals), np.round(epsilon,decimals), np.round(mu,decimals)